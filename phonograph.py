#!/usr/bin/env python3
from ev3dev.ev3 import *
from datetime import datetime
from datetime import timedelta
from threading import Timer
from time import sleep
import os.path;
import wave
import contextlib

class ToneArm():
	def __init__(self, horizontal_motor, horizontal_range, horizontal_speed, vertical_motor, vertical_range, vertical_speed, vertical_sensor, color_down):
		self.horizontal = LargeMotor(horizontal_motor)
		self.horizontal.reset()
		self.horizontal_range = horizontal_range
		self.horizontal_speed = horizontal_speed
		
		self.vertical = MediumMotor(vertical_motor)
		self.vertical_range = vertical_range
		self.vertical_speed = vertical_speed
		self.vertical_timeout = abs(vertical_range / vertical_speed) + 1
		
		self.vertical.reset()
		self.vertical.stop_action = MediumMotor.STOP_ACTION_BRAKE
		self.vertical.stop()
		
		self.sensor = ColorSensor(vertical_sensor)
		self.color_down = color_down
		self.sensor.mode = ColorSensor.MODE_COL_COLOR
		
		self.timer = None
		self.music = None
		self.Ready()
		
	def IsParked(self):
		return self.GetHorizontalPosition()/self.horizontal_range < 0.25
		
	def IsDown(self):
		return self.sensor.color != ColorSensor.COLOR_YELLOW
		
	def IsStopping(self):
		return self.is_stopping
	
	def Play(self, duration, filename):
		self.horizontal.stop_action = LargeMotor.STOP_ACTION_BRAKE
		self.horizontal.stop()
		
		self.play_start = self.GetHorizontalPosition();
		self.play_speed = (self.horizontal_range - self.play_start)/duration
		self.play_began = datetime.now()
		
		Sound.set_volume(100)
		self.music = Sound.play(filename)
		self.Move()
		
	def Move(self):
		passed = datetime.now() - self.play_began
		position = self.play_start + self.play_speed * passed.total_seconds()
		if abs(position) > abs(self.horizontal_range):
			position = self.horizontal_range
			
		self.horizontal.run_to_abs_pos(position_sp = position, speed_sp = self.horizontal_speed)
		
		if abs(position) < abs(self.horizontal_range):
			self.timer = Timer(1, self.Move)
			self.timer.start()
		
	def Stop(self):
		self.is_stopping = True
		self.Cancel()
		
		self.horizontal.stop_action = LargeMotor.STOP_ACTION_BRAKE
		self.horizontal.stop()
		
		self.vertical.run_to_rel_pos(position_sp = self.vertical_range, speed_sp = self.vertical_speed)
		self.timer = Timer(self.vertical_timeout, self.Park)
		self.timer.start()
		
	def Park(self):
		self.Cancel()
		self.horizontal.run_to_abs_pos(position_sp = 0, speed_sp = self.horizontal_speed)
		self.timer = Timer(abs(self.GetHorizontalPosition() / self.horizontal_speed) + 1, self.Ready)
		self.timer.start()
		
	def Ready(self):
		self.Cancel()
		self.horizontal.stop_action = LargeMotor.STOP_ACTION_COAST
		self.horizontal.stop()
		self.is_stopping = False
		
	def Cancel(self):
		if self.timer != None:
			if self.timer.is_alive():
				self.timer.cancel()
			self.timer = None
		if self.music != None:
			self.music.kill()
			
	def GetHorizontalPosition(self):
		while True:
			try:
				return self.horizontal.position
			except:
				continue


class Disc():
	COLOR_NONE = -1
	COLOR_RED = 0
	COLOR_BLACK = 1
	COLOR_GRAY = 2
	COLOR_YELLOW = 3
	
	LOOKUP_NONE = 0
	LOOKUP_FIRST = 1
	LOOKUP_SECOND = 2

	def __init__(self, motor, speed, tonearm, sensor, led, decoding, decoded):
		self.motor = LargeMotor(motor)
		self.motor.reset()
		self.motor.stop_action = LargeMotor.STOP_ACTION_COAST
		self.motor.stop()
		
		self.sensor = ColorSensor(sensor)
		self.sensor.mode = ColorSensor.MODE_RGB_RAW
		
		self.speed = speed
		self.tonearm = tonearm
		self.led = led
		self.decoding = decoding
		self.decoded = decoded
		
		self.running = False
		self.colorCode = ''
		self.lastColor = ''
		self.colorCount = 0
		self.lookup = self.LOOKUP_NONE
		self.Run()
		
	def Run(self):
		self.timer = Timer(0.5, self.Run)
		self.timer.start()
		
		deployed = not self.tonearm.IsParked()
		
		if self.lookup != self.LOOKUP_NONE:
			color = self.GetColor()
			if self.lastColor != color:
				self.lastColor = color
				if self.colorCount == 1: # Random color flashes, resetting
					self.lookup = self.LOOKUP_FIRST
				self.colorCount = 1
			else:
				self.colorCount = self.colorCount + 1
				if self.colorCount >= 2:
					if self.lookup == self.LOOKUP_FIRST:
						if color != 'r':
							self.colorCode = color
						elif self.colorCode != '':
							self.lookup = self.LOOKUP_SECOND
					else:
						if color != 'r':
							self.colorCode = self.colorCode + color
							self.lookup = self.LOOKUP_NONE
							Leds.set_color(self.led, self.decoded)
		
		if deployed == self.running:
			return
			
		self.running = deployed
		self.colorCode = ''
		
		if deployed:
			self.motor.run_forever(speed_sp = self.speed)
			self.lookup = self.LOOKUP_FIRST
			self.lastColor = ''
			self.colorCount = 0
			Leds.set_color(self.led, self.decoding)
		else:
			self.motor.stop()
			self.lookup = self.LOOKUP_NONE
		
	def GetColor(self):
		(r, g, b) = self.sensor.raw
		if r < 20 and g < 20 and b < 20: # Black
			return 'b'
		elif r > g*3 and r > b*3 and b >= g: # Red
			return 'r'
		elif r > g*1.6 and g > b*1.4: # Yellow
			return 'y'
		else: # Gray
			return 'g'
			
	def IsColorDecoded(self):
		return self.lookup == self.LOOKUP_NONE


class Player():
	def __init__(self, tonearm, stop_button, disc):
		self.tonearm = tonearm
		self.stop = TouchSensor(stop_button)
		self.disc = disc
		self.Start()
	
	def Start(self):
		if not self.tonearm.IsParked() and not self.tonearm.IsDown() and self.stop.value() == 1:
			self.tonearm.Park()
			self.timer = Timer(1, self.Start)
			self.timer.start()
		elif not self.tonearm.IsDown() or self.tonearm.IsParked() or self.tonearm.IsStopping() or not self.disc.IsColorDecoded():
			self.timer = Timer(1, self.Start)
			self.timer.start()
		else:
			self.FindFile()
			self.tonearm.Play(self.duration, self.filename)
			self.end = datetime.now() + timedelta(seconds = self.duration + 10)
			self.CheckForStop()
	
	def CheckForStop(self):
		if not self.tonearm.IsDown():
			self.tonearm.Ready()
			self.Start()
		elif self.stop.value() == 1 or datetime.now() > self.end:
			self.tonearm.Stop()
			self.Start()
		else:
			self.timer = Timer(0.25, self.CheckForStop)
			self.timer.start()
	
	def FindFile(self):
		self.filename = "default.wav"
		if os.path.isfile(self.disc.colorCode + ".wav"):
			self.filename = self.disc.colorCode + ".wav"
		
		self.duration = 60;
		with contextlib.closing(wave.open(self.filename, 'r')) as f:
			frames = f.getnframes()
			rate = f.getframerate()
			self.duration = frames / float(rate)
	

tonearm = ToneArm('outB', -40, 30, 'outA', -500, 120, 'in1', 1)
disc = Disc('outC', 60, tonearm, 'in2', Leds.RIGHT, Leds.ORANGE, Leds.GREEN)
player = Player(tonearm, 'in3', disc)

print("Running...")

while True:
	sleep(10)
